#   _               _              
#  | |__   __ _ ___| |__  _ __ ___ 
#  | '_ \ / _` / __| '_ \| '__/ __|
# _| |_) | (_| \__ \ | | | | | (__ 
#(_)_.__/ \__,_|___/_| |_|_|  \___|

###-ALIASES-###

# Improving output readability
alias grep='grep --color=auto'			# Add color to grep output
alias diff='diff --color=auto'			# Add color to diff output
alias ls='ls --color=auto'			# Add color to all ls commands
#alias cpv='cp -v'				# Verbose copy
alias cpv='rsync -ah --info=progress2'		# Less cluttered verbose copy using rsync

# Shortening common commands
alias c='clear'					# Quickly clear screen
alias ..='cd ..'				# Ascend 1 directory
alias .2='cd ../..'				# Ascend 2 directories
alias .3='cd ../../..'				# Ascend 3 directories

# Always needed flags
alias mkdir='mkdir -pv'				# Always create parent directories as needed
alias bc='bc -l'				# Launch bc with math support

# Simple added functionality
alias lmnt='mount | grep -E ^/dev | column -t'	# Simple view of mounted partitions
alias ghis='history | grep'			# Shortcut for grep-ing command history
alias lhis='history | less'			# Browser bash history with vim keys in less

# Safety nets
alias mv='mv -i'				# Interactive Move
alias rm='rm -I --preserve-root'		# Interactive Remove, never delete root
alias cp='cp -i'				# Interactive Copy

# List command abbreviations
alias la='ls -a'				# Listing hidden files
alias ll='ls -lhF --group-directories-first'	# List view
alias lla='ls -lhAF --group-directories-first'	# List view with hidden files
alias lt='ls -h --size -1 -S --classify'	# Sorting by file size  (add dir recursive?)
alias lta='ls -ha --size -1 -S --classify'	# Sorting by file size with hidden files
alias lh='ls -t -1'				# Sorting by last edited (reverse with -r)
alias lha='ls -ta -1'				# Sorting by last edited with hidden files

# VIM related
alias vi='vim'					# Use vim for vi
alias svi='sudo vim'				# Quickly open vim with super user rights

# Super User related / required
alias su='sudo -i bash'				# Login as Root with bash shell
alias reboot='sudo /sbin/reboot'		# Always preface reboot with sudo
alias poweroff='sudo /sbin/poweroff'		# Always preface poweroff with sudo
alias shutdown='sudo /sbin/shutdown'		# Always preface shtudown with sudo

# Git Bare Aliases
source $HOME/.gitbare

###-EXPORTS-###

# General
export EDITOR=vim
export VISUAL=vim
export PAGER=less
export TERM=st
export TERMINAL=st
export BROWSER=surf
export CLICOLOR=1
# XDG Directories
source $HOME/.config/user-dirs.dirs
# Scripts
export PATH=~/exe:$PATH
export EX=$HOME/exe/_ex
# NNN
export NNN_OPENER=/home/lh/exe/filesystem/openFile
export NNN_FIFO=/tmp/nnn.fifo
export NNN_FCOLORS='ca39121c103534f7c670abc4'
export NNN_TRASH=1
export NNN_PLUG='e:suedit;n:bulknew;d:diffs;g:dragdrop;h:hexview;t:treeview;p:preview-tabbed;'
export NNN_BMS='n:~/doc/Notes/;p:~/doc/Plans/;t:~/doc/ToDo/;e:~/exe/;i:~/med/img/;m:~/med/mus/;v:~/med/vid/;b:~/med/bok/;r:~/rep/;w:~/wrk/;'
# File / Link Opener
export OPN_WEB=surf
export OPN_DIR="$TERMINAL -e nnn"
export OPN_IMG=sxiv
export OPN_VID=mpv
export OPN_OFFICE=libreoffice
export OPN_PDF=mupdf

###-PS1-PROMPTS-###

#User->Elev->Host->Path->Git
#export PS1="【\u ⸢\\$⸥ \h \W \`parse_git_branch\` 】"

#User->Elev->Path->Git
export PS1="【\u ⸢\\$⸥ \W \`parse_git_branch\` 】"

#User->Elev->Path
#export PS1="【\u ⸢\\$⸥ \W 】"

#User->Path->Git
#export PS1="【\u \W \`parse_git_branch\`】"

#User->Path
#export PS1="【\u \W 】"


###-PS2-PROMPTS-###

export PS2="➔"
#export PS2="☛"

###-OTHER-PROMPTS-###
#(Add Later)


###-FUNCTIONS-###

# set terminal title
PROMPT_COMMAND='printf "\033]0;%s\007" "${PWD/#$HOME/"~"}"'

# get current branch in git repo
function parse_git_branch() {
	BRANCH=`git branch 2> /dev/null | sed -e '/^[^*]/d' -e 's/* \(.*\)/\1/'`
	if [ ! "${BRANCH}" == "" ]
	then
		STAT=`parse_git_dirty`
		echo "[${BRANCH}${STAT}]"
	else
		echo ""
	fi
}

# get current status of git repo
function parse_git_dirty {
	status=`git status 2>&1 | tee`
	dirty=`echo -n "${status}" 2> /dev/null | grep "modified:" &> /dev/null; echo "$?"`
	untracked=`echo -n "${status}" 2> /dev/null | grep "Untracked files" &> /dev/null; echo "$?"`
	ahead=`echo -n "${status}" 2> /dev/null | grep "Your branch is ahead of" &> /dev/null; echo "$?"`
	newfile=`echo -n "${status}" 2> /dev/null | grep "new file:" &> /dev/null; echo "$?"`
	renamed=`echo -n "${status}" 2> /dev/null | grep "renamed:" &> /dev/null; echo "$?"`
	deleted=`echo -n "${status}" 2> /dev/null | grep "deleted:" &> /dev/null; echo "$?"`
	bits=''
	if [ "${renamed}" == "0" ]; then
		bits=">${bits}"
	fi
	if [ "${ahead}" == "0" ]; then
		bits="*${bits}"
	fi
	if [ "${newfile}" == "0" ]; then
		bits="+${bits}"
	fi
	if [ "${untracked}" == "0" ]; then
		bits="?${bits}"
	fi
	if [ "${deleted}" == "0" ]; then
		bits="x${bits}"
	fi
	if [ "${dirty}" == "0" ]; then
		bits="!${bits}"
	fi
	if [ ! "${bits}" == "" ]; then
		echo " ${bits}"
	else
		echo ""
	fi
}


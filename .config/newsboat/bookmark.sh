#!/bin/sh

[ "$#" -eq 0 ] && exit 1
if [ -n $(command -v curl) ]; then
	  url=$(curl -sIL -o /dev/null -w '%{url_effective}' "$1")
  else  
	    url="$1"
fi
url=$(echo "${url}" | perl -p -e 's/(\?|\&)?utm_[a-z]+=[^\&]+//g;' -e 's/(#|\&)?utm_[a-z]+=[^\&]+//g;')
title="$2"
description="$3"

outputPath="$HOME/net/bookmarks/rss"
outputFile=Bookmarks-$(date +"%d.%m.%Y")
notify-send "Added Bookmark \"$title\" to \"$outputPath/$outputFile\""
grep -q "${url} ${title} ${description}" ~/.config/newsboat/bookmarks.txt || echo "${url} ${title} ${description}" >> "$outputPath/$outputFile"
